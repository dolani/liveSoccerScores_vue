# Dolani LiveScores

> A Vue.js project to feed live football/sports

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
=======

# Road Map
Signup with any livescore company to get your api details (api key) 

# Yet To implement
1. Beautify the Interface (UI)
2. Allow users to set favorite teams 	

