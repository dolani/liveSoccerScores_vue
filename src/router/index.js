import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Live from '@/components/Live.vue'
import League from '@/components/League'
import Favorite from '@/components/Favorite'
import axios from 'axios'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },

    {
      path: '/live',
      name: 'Live',
      component: Live
    },

    {
      path: '/league',
      name: 'League',
      component: League
    },

    {
      path: '/favorite',
      name: 'Favorite',
      component: Favorite
    }
  ]
})
